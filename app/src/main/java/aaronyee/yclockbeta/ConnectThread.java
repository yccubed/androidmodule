package aaronyee.yclockbeta;

import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.util.Log;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.UUID;
import java.io.OutputStream;


/**
 * Created by Aaron on 3/11/2016..
 */
public class ConnectThread extends Thread{

    private final BluetoothDevice bTDevice;
    private final BluetoothSocket bTSocket;


    public ConnectThread(BluetoothDevice bTDevice, UUID UUID) {
        BluetoothSocket tmp = null;
        this.bTDevice = bTDevice;

        try {
            tmp = this.bTDevice.createInsecureRfcommSocketToServiceRecord(UUID);
        }
        catch (IOException e) {
            Log.d("CONNECTTHREAD", "Could not start listening for RFCOMM");
        }
        bTSocket = tmp;
    }

    // TODO: connect handles send and receive even though methods are public
    public boolean connect() {

        try {
            bTSocket.connect();
        } catch(IOException e) {
            Log.d("CONNECTTHREAD", "Could not connect: " + e.toString());
            // seems to work with the first connect, but other people had problems online so adding this fallback socket just for safety
            try {
                BluetoothSocket fallback = null;
                Log.e("", "trying fallback...");
                try {
                    fallback = (BluetoothSocket) bTDevice.getClass().getMethod("createRfcommSocket", new Class[]{int.class}).invoke(bTDevice, 1);
                } catch (Exception exp) {
                    Log.d("CONNECTTHREAD", "could not assign fallback socket");
                }
                fallback.connect();
                Log.e("", "Connected");
            } catch (IOException exp) {
                Log.d("CONNECTTHREAD", "Could not connect to fallback: " + e.toString());
                try {
                    bTSocket.close();
                } catch (IOException close) {
                    Log.d("CONNECTTHREAD", "Could not close connection:" + e.toString());
                    return false;
                }
            }
        }
        return true;
    }

    public boolean cancel() {
        try {
            bTSocket.close();
        } catch(IOException e) {
            return false;
        }
        return true;
    }



    public boolean send(int num) {
        try {
            ByteArrayOutputStream output = new ByteArrayOutputStream(4);
            output.write(num);
            OutputStream outputStream = bTSocket.getOutputStream();
            outputStream.write(output.toByteArray());
        } catch (IOException e) {
            Log.d("SENDTHREAD", "cannot write outputStream:" + e.toString());
        }
        return true;
    }

    public int receive(){
        // TODO: only allows receiving and sending of one int right now
        try {
            byte[] buffer = new byte[4];
            ByteArrayInputStream input = new ByteArrayInputStream(buffer);
            InputStream inputStream = bTSocket.getInputStream();
            inputStream.read(buffer);
            return input.read();
        } catch (IOException e) {
            Log.d("SENDTHREAD", "cannot read InputStream:" + e.toString());
        }
        return -1;
    }

}
