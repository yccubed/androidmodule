package aaronyee.yclockbeta;

import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.MotionEvent;
import android.text.format.Time;
import android.os.SystemClock;

import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;


public class HomeScreen extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client2;

    private int count = 0;
    private int recint;
    private int sendint;
    private BluetoothAdapter mybta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2 = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public void turnOnBluetooth(View view) {
        mybta = BluetoothAdapter.getDefaultAdapter();
        mybta.enable();
    }

    public void turnOffBluetooth(View view)
    {
        mybta = BluetoothAdapter.getDefaultAdapter();
        mybta.disable();
    }

    public void connect_to_pi(View view) {
        UUID lockID = UUID.fromString("45ec5393-c64c-f33c-3a8f-258874c0f198");
        mybta = BluetoothAdapter.getDefaultAdapter();
        ArrayList<BluetoothDevice> btItemList = new ArrayList<BluetoothDevice>();
        Set<BluetoothDevice> pairedDevices = mybta.getBondedDevices();
        for (BluetoothDevice device : pairedDevices) {
            btItemList.add(device);
        }
        final ConnectThread connect;
        for (int i = 0; i < btItemList.size(); i++) {
            //TODO: RPi Bluetooth Adapter MAC Address hardcoded, need to make dynamic for scalability
            if (btItemList.get(i).getAddress().equals("00:19:86:00:12:41")) {
                connect = new ConnectThread(btItemList.get(i), lockID);
                connect.connect();
                recint = connect.receive();
                // Instantiate an AlertDialog.Builder with its constructor
                AlertDialog.Builder noBT = new AlertDialog.Builder(this);
                // Chain together various setter methods to set the dialog characteristics
                // TODO: Create a better Dialog box
                noBT.setTitle("Approved User?")
                        .setMessage("What is the correct number? LockYC sends:" + recint)
                        .setPositiveButton(Integer.toString(recint * recint * recint), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                sendint = (recint * recint * recint);
                                connect.send(sendint);
                            }
                        })
                        .setNegativeButton(Integer.toString(recint * recint), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                                System.exit(0);
                            }
                        })
                        .create().show();
                // Called create() on the object and then call show() just to be safe,
                // but also works with just show(). Not sure what the implications of this are.
                break;
            }
        }


    }

    public void who_home(View view)
    {
        Intent emailIntent = new Intent();
        emailIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");// Package Name, Class Name
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"yc3lock@gmail.com"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Who is Home" +" " + count);
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Who is Home");
        count += 1;
        startActivity(emailIntent);
    }

    public void lock_the_door(View view)
    {
        Intent emailIntent = new Intent();
        emailIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");// Package Name, Class Name
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"yc3lock@gmail.com"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Lock the Door" +" " + count);
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Lock the Door");
        count += 1;
        startActivity(emailIntent);
    }
    public void unlock_the_door(View view)
    {
        Intent emailIntent = new Intent();
        emailIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");// Package Name, Class Name
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"yc3lock@gmail.com"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Open the Door" +" " + count);
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Open the Door");
        count += 1;
        startActivity(emailIntent);
    }


    public void take_snapshot(View view) {
//        BluetoothAdapter mybta = BluetoothAdapter.getDefaultAdapter();
//        String test1 = mybta.getAddress();
//        String test2 = mybta.getName();
//        Log.d("test1",test1);
//        Log.d("test2", test2);
//        BluetoothDevice mybtd = mybta.getRemoteDevice("sdfjkl");
//        BluetoothSocket mybts = mybtd.createRfcommSocketToServiceRecord()
/*        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL  , new String[]{"aaron.yee.1994@gmail.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "Test Command");
        i.putExtra(Intent.EXTRA_TEXT   , "Do this pls");
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(HomeScreen.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();

        }*/

        Intent emailIntent = new Intent();
        emailIntent.setClassName("com.google.android.gm", "com.google.android.gm.ComposeActivityGmail");// Package Name, Class Name
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"yc3lock@gmail.com"});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Take Snapshot" +" " + count);
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Take Snapshot");
        count += 1;
        startActivity(emailIntent);




    }


    @Override
    public void onStart() {
        super.onStart();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client2.connect();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "HomeScreen Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://aaronyee.yclockbeta/http/host/path")
        );
        AppIndex.AppIndexApi.start(client2, viewAction);
    }


    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "HomeScreen Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://aaronyee.yclockbeta/http/host/path")
        );
        AppIndex.AppIndexApi.end(client2, viewAction);
        client2.disconnect();
    }
}
